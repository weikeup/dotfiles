# Dotfiles

Select OS: **Windows** | [Linux](https://gitlab.com/weikeup/dotfiles/-/tree/linux)

## Setup

### Install Apps

_Note: Restart terminal after install apps._

- Install the latest PowerShell

  ```powershell
  winget install PowerShell -s winget
  ```

- Install NeoVim

  ```powershell
  winget install NeoVim
  ```

- Install z

  ```powershell
  Install-Module z
  ```

- Install Oh-My-Posh

  ```powershell
  winget install JanDeDobbeleer.OhMyPosh -s winget
  ```

### Configuration

- Setup Oh-My-Posh

  ```powershell
  echo 'oh-my-posh init pwsh --config ~\.config\ohmyposh\di4am0nd.omp.json | Invoke-Expression' >> $PROFILE
  ```
